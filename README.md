# Minha configuração neovim

Esta é a minha configuração pessoal do neovim, feita com base no [kickstarter.nvim](https://github.com/nvim-lua/kickstart.nvim)

#### Olha a carinha dela:
<br>
<a href="https://ibb.co/2MPjqsT"><img src="https://i.ibb.co/kD0hK9t/preview.png" alt="preview" border="0"></a>
<br>
<a href="https://ibb.co/PthjR4r"><img src="https://i.ibb.co/JC7tN2c/Tela-inicial.png" alt="Tela-inicial" border="0"></a>
<br>
<a href="https://ibb.co/GVsPhX6"><img src="https://i.ibb.co/hKX8qk6/Terminal.png" alt="Terminal" border="0"></a>

# Aqui você vai encontrar 
## - Suporte a LSP, vim-cmp e snippets!  

<a href="https://imgbb.com/"><img src="https://i.ibb.co/2hFSRgb/cmp-lsp.png" alt="cmp-lsp" border="0"></a>  
## - Suporte a TreeSitter e Muitos Temas!

<a href="https://ibb.co/BzRzY0Q"><img src="https://i.ibb.co/0D3DzH6/Colorschemes.png" alt="Colorschemes" border="0"></a>  
## - Gerenciador de plugins (Lazy.nvim)  

<a href="https://ibb.co/tpG8YCC"><img src="https://i.ibb.co/4RqP4NN/Lazy.png" alt="Lazy" border="0"></a>  

## - <s>Suporte a Debugers</s> Em desenvolvimento...

### Atalhos
#### no modo normal:
- espaço + e: abre o explorador do arquivos
- espaço duas vezes: abre a busca de arquivos 
- espaço + t: abre o terminal
- s duas vezes: divide a tela no meio horizontalmente
- v duas vezes: divide a tela no meio verticalmente
- ctrl + teclas de navegação do vim (hjkl): Foca em um buffer na direção que você escolher (janela)

Para mais, aperte a tecla espaço e aguarde um pouquinho, você vai ser apresentado com todos os atalhos

Ao entrar no modo de comando (apertando ":"), você pode usar a integração com o git, digitando os comandos que você usaria no terminal normalmente, porem, note que você deve digitar "Git" e não "git"

# Configurando
No arquivo ~/.config/nvim/lua/options.lua, você pode editar as configurações

Para baixar um tema, vá em ~/.config/nvim/lua/custom/plugins/colorscheme.lua, e siga as instruções
Para difinir ele como tema padrão, vá no arquivo ~/.config/nvim/init.lua e edite a linha 12
Você pode testar um tema rapidamente usando ```:colorscheme nome do tema```

# Instalação

Você pode instalar essa configuração apartir deste one-liner:

```git clone https://gitlab.com/davilooopes/meu-neovim.git ~/.config/nvim```

# Atualizando
Entre em ~/.config/nvim e digite "git pull"

# Desinstalando
Remova a pasta ~/.config/nvim
