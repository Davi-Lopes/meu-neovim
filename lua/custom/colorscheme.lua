return {
  'navarasu/onedark.nvim', -- Troque isso pelo nome de usuário/nome do repositório do github do tema
  priority = 1000,
  config = function() -- Configurações do tema, se não quiser mexer em nada, comente esta função 
    require('onedark').setup {
      style = 'deep'
    }
  end,
}
